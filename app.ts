import * as bodyParser from "body-parser";
import * as express from "express";
import * as fs from "fs";
import * as moment from "moment";
import * as winston from "winston";
import { create, CreateOptions } from "html-pdf";

const app = express();

winston.configure({
    level: 'debug',
    transports: [
        new (winston.transports.Console)({
            timestamp: function () {
                return moment().format('YYYY-MM-DD hh:mm:ss');
            },
            formatter: function (options) {
                // Return string will be passed to logger.
                return options.timestamp() + ' ' + options.level.toUpperCase() + ': ' + (options.message ? options.message : '') +
                    (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '' );
            }
        })
    ]
});

app.use(bodyParser.json());
app.use(express.static('public'));

app.get('/', (req, res) => {
    res.send('Site is functioning correctly.')
});

app.post('/html-to-pdf/:name', (req, res) => {
    winston.debug('Received convert message.', req.body);

    if (!req.body) {
        winston.debug('No body in message. Responding with error 400.');
        res.status(400).send('No message body found.');
        return;
    }

    const name = req.params.name.replace(/(?:.pdf)+$/, '');
    winston.debug(`NAME = ${name}`);
    const docsDir = './public/documents';
    let output = `${docsDir}/${name}.pdf`;
    if (!output.endsWith('.pdf')) {
        output += '.pdf';
    }

    fs.exists(docsDir, (exists) => {
        if (!exists) {
            winston.debug("The documents directory doesn't exist. Creating it.");
            fs.mkdir(docsDir, err => {
                if (err) {
                    winston.error(err.message);
                } else {
                    winston.debug('Documents directory created.');
                    createPdf();
                }
            });
        } else {
            createPdf()
        }
    });

    const createPdf = () => {
        fs.exists(output, (exists) => {
            if (exists) {
                winston.debug('Pdf exists. Returning cached file.');
                res.download(output);
            } else {
                if (!req.body.data) {
                    winston.debug('No data found in message body. Responding with error 400.');
                    res.status(400).send('No pdf data found in the message body. Send a JSON object with html content in the "data" property.');
                    return;
                }

                winston.debug('Building pdf.');
                const pdfOptions: CreateOptions = {
                    format: 'A4'
                };

                create(req.body.data, pdfOptions).toFile(output, (error, result)=> {
                    if (error) {
                        winston.debug('Building pdf failed.');
                        winston.debug(error.message);
                        res.sendStatus(500);
                        return;
                    }

                    winston.debug(JSON.stringify(result));
                    winston.debug('Pdf generation complete.');
                    res.download(output);
                });
            }
        });
    };
});

app.listen(process.env.PORT || 3000, () => {
    winston.info(`App listening on port ${process.env.PORT || 3000}!`);
});