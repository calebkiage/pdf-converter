"use strict";
var bodyParser = require("body-parser");
var express = require("express");
var fs = require("fs");
var moment = require("moment");
var winston = require("winston");
var html_pdf_1 = require("html-pdf");
var app = express();
winston.configure({
    level: 'debug',
    transports: [
        new (winston.transports.Console)({
            timestamp: function () {
                return moment().format('YYYY-MM-DD hh:mm:ss');
            },
            formatter: function (options) {
                // Return string will be passed to logger.
                return options.timestamp() + ' ' + options.level.toUpperCase() + ': ' + (options.message ? options.message : '') +
                    (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '');
            }
        })
    ]
});
app.use(bodyParser.json());
app.use(express.static('public'));
app.get('/', function (req, res) {
    res.send('Site is functioning correctly.');
});
app.post('/html-to-pdf/:name', function (req, res) {
    winston.debug('Received convert message.', req.body);
    if (!req.body) {
        winston.debug('No body in message. Responding with error 400.');
        res.status(400).send('No message body found.');
        return;
    }
    var name = req.params.name.replace(/(?:.pdf)+$/, '');
    winston.debug("NAME = " + name);
    var docsDir = './public/documents';
    var output = docsDir + "/" + name + ".pdf";
    if (!output.endsWith('.pdf')) {
        output += '.pdf';
    }
    fs.exists(docsDir, function (exists) {
        if (!exists) {
            winston.debug("The documents directory doesn't exist. Creating it.");
            fs.mkdir(docsDir, function (err) {
                if (err) {
                    winston.error(err.message);
                }
                else {
                    winston.debug('Documents directory created.');
                    createPdf();
                }
            });
        }
        else {
            createPdf();
        }
    });
    var createPdf = function () {
        fs.exists(output, function (exists) {
            if (exists) {
                winston.debug('Pdf exists. Returning cached file.');
                res.download(output);
            }
            else {
                if (!req.body.data) {
                    winston.debug('No data found in message body. Responding with error 400.');
                    res.status(400).send('No pdf data found in the message body. Send a JSON object with html content in the "data" property.');
                    return;
                }
                winston.debug('Building pdf.');
                var pdfOptions = {
                    format: 'A4'
                };
                html_pdf_1.create(req.body.data, pdfOptions).toFile(output, function (error, result) {
                    if (error) {
                        winston.debug('Building pdf failed.');
                        winston.debug(error.message);
                        res.sendStatus(500);
                        return;
                    }
                    winston.debug(JSON.stringify(result));
                    winston.debug('Pdf generation complete.');
                    res.download(output);
                });
            }
        });
    };
});
app.listen(process.env.PORT || 3000, function () {
    winston.info("App listening on port " + (process.env.PORT || 3000) + "!");
});
//# sourceMappingURL=app.js.map